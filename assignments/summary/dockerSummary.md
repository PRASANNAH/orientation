**Docker**
Docker is a software platform for building applications based on containers — small and lightweight execution environments that make shared use of the operating system kernel but otherwise run in isolation from one another.

A few Docker commands are:
1. docker –version
This command is used to get the currently installed version of docker

2. docker pull
Usage: docker pull <image name>
This command is used to pull images from the docker repository(hub.docker.com)

3. docker run
Usage: docker run -it -d <image name>
This command is used to create a container from an image

4. docker ps
This command is used to list the running containers

5. docker ps -a
This command is used to show all the running and exited containers

6. docker exec
Usage: docker exec -it <container id> bash
This command is used to access the running container

7. docker stop
Usage: docker stop <container id>
This command stops a running container

8. docker kill
Usage: docker kill <container id>
This command kills the container by stopping its execution immediately. 

9. docker commit
Usage: docker commit <conatainer id> <username/imagename>
This command creates a new image of an edited container on the local system

10. docker login
This command is used to login to the docker hub repository

**Docker Teminology** :

1. Docker

Docker is a program for developers to develop, and run applications with containers.


2. Docker Image

A Docker image is contains everything needed to run an applications as a container. This includes:

code
runtime
libraries
environment variables
configuration files


The image can then be deployed to any Docker environment and as a container.


3. Container

A Docker container is a running Docker image.
From one image you can create multiple containers .


4. Docker Hub

Docker Hub is like GitHub but for docker images and containers.



