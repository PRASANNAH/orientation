Git Basics:

Life as a software developer is social and collaborative–sometimes to the point of frustration. That’s why we have git and GitLab.
Git lets you easily keep track of every revision you and your team make during the development of your software.

Clone:

Cloning a repo is pretty much exactly what it sounds like. It takes the entire online repository and makes an exact copy of it on your local machine.

Fork:

Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.

Commit:

Think of this as saving your work. When you commit to a repository, it’s like you’re taking picture/snapshot of the files as they exist at that moment. The commit will only exist on your local machine until it is pushed to a remote repository.

Push:

Pushing is essentially syncing your commits to GitLab.


Basic overview of how Git works:


- create a "repository" (project) with a git hosting tool (like Bitbucket)
- Copy (or clone) the repository to your local machine

- Add a file to your local repo and "commit" (save) the changes

- "Push" your changes to your master branch

- Make a change to your file with a git hosting tool and commit

- "Pull" the changes to your local machine

- Create a "branch" (version), make a change, commit the change

- Open a "pull request" (propose changes to the master branch)

- "Merge" your branch to the master branch

Git Commands :


    git config
    git init
    git clone
    git add
    git commit
    git diff
    git reset
    git status
    git rm
    git log
    git show
    git tag
    git branch
    git checkout
    git merge
    git remote
    git push
    git pull
    git stash
